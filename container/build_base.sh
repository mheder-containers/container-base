#!/bin/sh
set -eux

ansible_chroot_host ()
{
cat <<EOF > container/ansible/chroot_host
[chroot_dir]
./"${os_release}"
EOF
}
build_container ()
{
  debootstrap --components=main,contrib,non-free --include=apt,bash,ca-certificates,dpkg,mount,python,python-minimal "${os_release}" "${os_release}"
  chroot "${os_release}" mount -t proc proc /proc
  chroot "${os_release}" mount -t sysfs sysfs /sys
  chroot "${os_release}" mount -t devtmpfs udev /dev
  chroot "${os_release}" mount -t devpts devpts /dev/pts
  chroot "${os_release}" mount -t tmpfs tmpfs /dev/shm
  chroot "${os_release}" update-ca-certificates
  ansible_chroot_host
  ansible-playbook -i container/ansible/chroot_host container/ansible/base.yml
  chroot "${os_release}" umount /proc /sys /dev/pts /dev/shm /dev
  mount -t proc proc /proc
}

if [ -z "${CI_COMMIT_TAG+x}" ] ; then
  for os_release in buster
  do
    mkdir -p "${os_release}"
    build_container
    target_image="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-${CONTAINER_NAME_POSTFIX}:${CI_COMMIT_REF_NAME}"
    tar -c --directory "${os_release}" . | podman import - "${target_image}"
    buildah push "${target_image}"
  done
else
  for os_release in buster
  do
    target_image_base="${CI_REGISTRY}/${CI_PROJECT_PATH}/${os_release}-${CONTAINER_NAME_POSTFIX}"
    major_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1)
    minor_tag=$(echo "${CI_COMMIT_TAG}" | cut -d. -f1,2)
    patch_tag=$(echo "${CI_COMMIT_TAG}" | grep -E '^v[0-9]+\.[0-9]+(\.[0-9]+)?(-.+)?$')
      if [ -z "${patch_tag}" ] ; then
        printf 'Error: the latest '"${CI_COMMIT_TAG}"' tag is not starts with v[0-9].[0-9]%s\n'
        exit 1
      fi
    build_container
    tar -c --directory "${os_release}" . | podman import - "${target_image_base}:tagged"
    for tag in "${major_tag}" "${minor_tag}" "${patch_tag}"
    do
      buildah tag "${target_image_base}:tagged" "${target_image_base}:${tag}"
      buildah push "${target_image_base}:${tag}"
    done
  done
fi
